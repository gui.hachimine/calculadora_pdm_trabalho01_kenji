package com.example.pdm_calculadora_trabalho01_kenji;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

//Aluno: Guilherme Kenji Ferreira Hachimine
//RA:36039303808-1
public class MainActivity extends AppCompatActivity {
    private Button n1;
    private Button n2;
    private Button n3;
    private Button n4;
    private Button n5;
    private Button n6;
    private Button n7;
    private Button n8;
    private Button n9;
    private Button n0;
    private Button soma;
    private Button div;
    private Button sub;
    private Button vezes;
    private Button result;
    private Button clear;
    private Button virg;
    private TextView entrada;
    private TextView hist;
    private char operacao;
    private double value01= Double.NaN;//Inseri um valor que não é número para utilizar como validação de entrada
    private double value02;
    private double resultado=0;
    private String aux = "0";
    private boolean ponto = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar;
        actionBar = getSupportActionBar();
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#808080"));
        actionBar.setBackgroundDrawable(colorDrawable);
        n1      = findViewById(R.id.bt_n1);
        n2      = findViewById(R.id.bt_n2);
        n3      = findViewById(R.id.bt_n3);
        n4      = findViewById(R.id.bt_n4);
        n5      = findViewById(R.id.bt_n5);
        n6      = findViewById(R.id.bt_n6);
        n7      = findViewById(R.id.bt_n7);
        n8      = findViewById(R.id.bt_n8);
        n9      = findViewById(R.id.bt_n9);
        n0      = findViewById(R.id.bt_n0);
        soma    = findViewById(R.id.bt_soma);
        div     = findViewById(R.id.bt_div);
        sub     = findViewById(R.id.bt_sub);
        vezes   = findViewById(R.id.bt_vezes);
        result  = findViewById(R.id.bt_result);
        clear   = findViewById(R.id.bt_clear);
        virg    = findViewById(R.id.bt_virgula);
        entrada = findViewById(R.id.txt_entrada);
        hist = findViewById(R.id.txt_historico);


        n1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"1");
            }
        });

        n2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"2");
            }
        });

        n3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"3");
                entrada.setText(entrada.getText().toString()+"");
            }
        });

        n4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"4");
            }
        });

        n5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"5");
            }
        });

        n6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"6");
            }
        });

        n7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"7");
            }
        });

        n8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"8");
            }
        });

        n9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aux = removeZeroaesquerda(entrada.getText().toString());
                //resolvendo problema com a virgula e 0
                if(aux.contains(".") && aux.indexOf(".")==0)
                {
                    aux =  ("0" + aux.toString());
                }
                entrada.setText(aux+"9");
            }
        });

        n0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //resolvendo problema com a virgula e 0
                if(entrada.toString().contains(".") && entrada.toString().indexOf(".")==0)
                {
                    entrada.setText  ("0." +entrada.toString());
                }
                    entrada.setText(entrada.getText().toString()+"0");


            }
        });

        soma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculo();
                operacao = '+';
                verificaInteiro();

                entrada.setText(null);
                ponto=false;
            }
        });

        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculo();
                operacao = '/';
                verificaInteiro();
                entrada.setText(null);
                ponto=false;
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculo();
                operacao = '-';
                verificaInteiro();
                entrada.setText(null);
                ponto=false;
            }
        });

        vezes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculo();
                operacao = 'x';
                verificaInteiro();
                entrada.setText(null);
                ponto=false;
            }
        });

        result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculo();
                operacao = '=';
                ponto=false;
                if(resultado%1==0)
                {
                    int aux_int= (int)resultado;
                    entrada.setText(String.valueOf(aux_int));
                }
                else
                {
                    entrada.setText(String.valueOf(resultado));
                }
                if(value02%1==0)
                {
                    int aux_int = (int)value02;
                    hist.setText(hist.getText().toString()+String.valueOf(aux_int)+"=");
                }
                else
                {
                    hist.setText(hist.getText().toString()+String.valueOf(value02)+"=");
                }



            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrada.setText("0");

                aux = "0";
                ponto = false;
                hist.setText("");
                value01= Double.NaN;//Inseri um valor que não é número para utilizar como validação de entrada
                value02 = Double.NaN;
                resultado=Double.NaN;
            }
        });

        virg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ponto)
                {
                    String aux = removeZeroaesquerda(entrada.getText().toString());
                    if(aux.isEmpty())
                    {
                        aux="0";
                    }
                    entrada.setText(aux+".");
                    ponto=true;
                }

            }
        });
    }

    private void calculo()
    {
        if(!Double.isNaN(value01))//isNaN verifica se o valor não é um número, portanto, com ! a verificação vê se é um número
        {

            value02 = Double.parseDouble(entrada.getText().toString());
            if(!Double.isNaN(resultado))
            {
                value01 = resultado;
            }
            switch (operacao)
            {
                case '+':
                    resultado = value01+value02;
                    break;

                case '-':
                    resultado = value01-value02;
                    break;

                case '/':
                    resultado = value01/value02;
                    break;

                case 'x':
                    resultado = value01*value02;
                    break;
                case '=':
                    break;


            }
        }
        else//caso value01 não seja um número, ou seja caso não tenha sido instanciado
        {
            value01 = Double.parseDouble(entrada.getText().toString());
            resultado = value01;
        }
    }


        public static String removeZeroaesquerda(String num)
        {

            int i = 0;

            while ((i < num.length()) && (num.charAt(i) == '0'))
                i++;

            StringBuffer novaString = new StringBuffer(num);

            // The  StringBuffer replace function removes
            // i characters from given index (0 here)

                novaString.replace(0, i, "");

            return novaString.toString();  // return in String
        }

        public void verificaInteiro()
        {
            if(resultado%1==0)
            {
                int aux_int= (int)resultado;
                hist.setText(String.valueOf(aux_int)+operacao);
            }
            else
            {
                hist.setText(String.valueOf(resultado)+operacao);
            }
        }
}